![PRISM Logo](derby_logo.svg)

# Table of Contents

1. [Manual Build Instructions](#manual-build-instructions)
   1. [Manual Build Pre Requisites](#manual-build-pre-requisites)
   2. [Project Bootstrap](#project-bootstrap)
   3. [Run a Development Server](#run-a-development-server)
   4. [Docker Image Build](#docker-image-build)
   5. [Troubleshooting](#troubleshooting)
2. [Automated Build Instructions](#automated-build-instructions)
   1. [Automated Build Pre Requisites](#automated-build-pre-requisites)
   2. [Options for Running the Script](#options-for-running-the-script)
      1. [Runnind the Script - Default](#running-the-script---default)
      2. [Runnind the Script - Debug](#running-the-script---debug)
      3. [Runnind the Script - Build](#running-the-script---build)
3. [PRISM Developer Notes](#prism-developer-notes)
   1. [CSS](#css)
   2. [Constants](#constants)
   3. [PRISM Source Code Changes](#prism-source-code-changes)

# Manual Build Instructions:

These Build Instructions are based on the Kibana Developers Guide which can be found [here](https://github.com/elastic/kibana/blob/main/dev_docs/getting_started/setting_up_a_development_env.mdx).

[click here](#automated-build-instructions) for an automated build option.

## Manual Build Pre Requisites

1. Be sure that you have docker installed and that your user is in the docker group. If you need to add the user that you're logged in as to the docker group run the following command and then restart.

```
sudo usermod -aG docker $USER
```

2. Install node.js version 10.23.1. If you have nvm setup then you can use the following command to install the required version of node.js

```
nvm install v10.23.1
```

3. Install yarn v1.22.21

```
npm install -g yarn && yarn set version 1.22.21
```

## Project Bootstrap

1. Clone the **_derbyinsights/derby-prism_** repo into you're local environment

```
git clone git@gitlab.com:derbyinsight/derby-prism.git
```

2. Bootstrap the project (_**NOTE**: this can take a few min to run_)

```
yarn kbn bootstrap
```

## Run a Development Server

If you'd like to update the code, you can setup a local development environment after bootstrapping with the following steps:

1. Start a local Elasticsearch Instance (be sure that nothing else is running on port 9200)

```
yarn es snapshot --version 7.10.2
```

2. Once the Elasticsearch instance is running run the following to start an instance of PRISM Analytics (_**NOTE:** be sure that you are in the root project directory_).

```
yarn start
```

Once the PRISM Analytics server is started, the logs will display the URL that you can use in the browser for PRISM Analytics. It will look something like the following:

```
[server][Kibana][http] http server running at http://localhost:5603/jrd
```

You should be able to login with the following credentials:

**Username:** elastic

**Password:** changeme

## Docker Image Build

Run the following command to build a docker image of Derby analytics.

```
yarn build --docker --no-oss --skip-docker-ubi
```

This will add an image to Docker called **docker.elastic.co/kibana/kibana:7.10.2-SNAPSHOT**

you can now tag the image for PRISM Analytics

```
docker tag docker.elastic.co/kibana/kibana:7.10.2-SNAPSHOT registry.derbyinsight.com:5000/kibana-derby:7.10.2-n
```

## Troubleshooting

If there is an issue with the build related to centos the following should fix the issue. This shouldn't be necessary but is here for reference. (_**NOTE:** a Pre requisite for this is that the bootstrap has successfully completed_)

1. Navigate to the following directory in the project and open the Dockerfile

```
cd src/dev/build/tasks/os_packages/docker_generator/templates/
```

2. Add the following after **EXPOSE 5601** in the Dockerfile

```
RUN cd /etc/yum.repos.d/
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
```

3. After taking this step re-run the build

```
yarn build --docker --no-oss --skip-docker-ubi
```

# Automated Build Instructions

The automated build checks and installs if necessary the required versions of nvm, node and yarn. It also automatically runs the prism environment bootstrap (similar to yarn install or npm install). Finally, if specified a docker image will be built for PRISM.

## Automated Build Pre Requisites

Prior to running the automated build you need to have **curl** and **docker** installed. It is assumed that you have **git** installed since this is a git repository.

## Options for Running the Script

There are a few different options for running the script. If **debug** is passed then more information on the progress of the script is logged to the terminal. If **build** is passed and a modern version of docker is installed then a docker image for PRISM will be built and added to your docker images for your use.

_**NOTE:** The script will prompt the user for confirmation that they realize that nvm, node and or yarn could be installed in their environment._

### Running the Script - Default

In order to run the default build which checks for the correct versions of nvm, node and yarn and bootstraps the project so that it can either be built or further customized in the code you can run the following command.

```
./prism_setup.sh
```

### Running the Script - Debug

This will perform the exact same functions as the Default build but will include some further debug information on the terminal.

```
./prism_setup.sh debug
```

### Running the Script - Build

This will perform the exact same functions as the Default build but with one final step which will build a docker image for PRISM. Debug can also be passed or not based on your preference.

#### With Debug

```
./prism_setup.sh debug build
```

#### Without Debug

```
./prism_setup.sh build
```

# PRISM Developer Notes

PRISM is a custom extension of Kibana taken at the last release before Kibana's licensing changed which is v7.10.2. The following section is intended to be helpful for developers interested in further customizing Kibana for PRISM.

## CSS

The Kibana codebase uses scss. All custom CSS for the PRISM skin can be found in the **/derby_css** folder and the contained files **derby_styles.scss** and **derby_variables.scss**. In most cases these styles are applied without the need of import but in the following cases the derby_styles.scss file is imported into the typescript and or scss files:

```
src/core/public/application/ui/app_container.scss
```

```
src/core/public/chrome/ui/header/header_logo.tsx
```

```
src/plugins/saved_objects_management/public/management_section/objects_table/components/flyout.tsx
```

```
x-pack/plugins/security/public/authentication/login/components/login_form/login_form.scss
```

## Constants

There are 3 key data items that were used throughout the customization of Kibana for the PRISM skin. Those were base64 encoded values for the Prism Analytics Logo and Icon and the application name in replace of Kibana which is 'Prism Analytics'.

In order to help simplify future maintenance the values for these 3 data items have been coded as constants and then used in the code where needed. Due to the complex configuration of the build for Kibana, it was not straightforward as to how to put these 3 constants in one file referenced throughout the rest of the source. Kibana is setup with TypeScript such that there are multiple TypeScript applications that each draw boundaries around what folders are allowed to contain source code for that application and all applications are effectively composites of the main Kibana application. The following is a list of the constants.ts files that have been used to get around this complex build structure of Kibana and while the implication is that we have replication of code, this seemed manageable when all things were considered:

```
src/core/derby/constants.tsx
```

```
src/plugins/derby/constants.tsx
```

```
src/plugins/kibana_react/public/derby/constants.tsx
```

```
x-pack/derby/constants.tsx
```

_**NOTE**: there is probably a way to consolidate these constant files into 1 that is used throughout PRISM but decided that this is something to consider at a later point._

## PRISM Source Code Changes

Finally, there were 69 files that were updated to customize Kibana for PRISM Analytics. Each change can be easily found by searching the source for the following string:

```
// DERBY
```
